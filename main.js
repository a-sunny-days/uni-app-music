import App from './App'
import Vue from 'vue'
import store from 'store/index.js'
// import Vconsole from './vconsole.js'
// Vue.prototype.$vconsole = new vconsole() // 使用vconsole

// #ifndef VUE3

Vue.config.productionTip = false

App.mpType = 'app'
//添加过滤器
Vue.filter('formatCount', function(value) {
	if (value >= 10000 && value <= 100000000) {
		value /= 10000;
		//保留一位小数
		return value.toFixed(1) + '万'
	} else if (value >= 100000000) {
		value /= 100000000;
		return value.toFixed(1) + '亿'
	} else {
		return value;
	}
});
//定义过滤器
Vue.filter('formatTime', function(value) {
	var date = new Date(value);
	//返回的时间
	return date.getFullYear() + '年' + (date.getMonth() + 1) + '月' + date.getDate() + '日';
})
const app = new Vue({
	...App,
	store
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif

