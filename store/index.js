// 引入
import Vue from "vue"
import Vuex from 'vuex'
Vue.use(Vuex);
export default new Vuex.Store({
	// 初始数据
	state: {
		//全局共享数据
		topListIds: [],
		nextId: ''
	},
	mutations: {
		INIT_TOPLISTIDS(state, payload) {
			state.topListIds = payload;
		},
		NEXT_ID(state, payload) {
			//payload当前的id
			for (var i = 0; i < state.topListIds.length; i++) {
				if (state.topListIds[i].id == payload) {
					state.nextId = state.topListIds[i + 1].id;
				}
			}
		}
	}
})
