//引入
import {
	baseUrl
} from './config.js'

//4大排行榜接口
export function topList() {
	let listIds=['19723756','3779629','2884035','3778678'];
	return new Promise(function(resolve, reject) {
		uni.request({
			// api接口地址拼接
			url: `${baseUrl}/toplist/detail`,
			method: 'GET',
			data: {},
			success: res => {
				let result=res.data.list;
				//取前4项
				// console.log(res);
				result.length=4;
				for(var i=0;i<listIds.length;i++){
					//对应歌单每一项的id
					result[i].listId=listIds[i];
				}
				resolve(result)
			},
			fail: () => {},
			complete: () => {}
		})
	});
}
//歌单详情接口
export function list(listId){
	//promise对象
 return	uni.request({
		// 地址拼接
		url:`${baseUrl}/playlist/detail?id=${listId}`,
		method:'GET'
	});
}
// http://localhost:3000/song/detail?ids=483937795 //歌曲对应封面名字
// 封装接口
export function songDetail(songId){
	//promise对象
 return	uni.request({
		// 地址拼接
		url:`${baseUrl}/song/detail?ids=${songId}`,
		method:'GET'
	});
}
// http://localhost:3000/simi/song/?id=483937795 //相似歌曲推荐
export function songSimi(songId){
	//promise对象
 return	uni.request({
		// 地址拼接
		url:`${baseUrl}/simi/song/?id=${songId}`,
		method:'GET'
	});
}
// http://localhost:3000/comment/music?id=483937795 //评论区
export function songComment(songId){
	//promise对象
 return	uni.request({
		// 地址拼接
		url:`${baseUrl}/comment/music?id=${songId}`,
		method:'GET'
	});
}
// http://localhost:3000/lyric?id=483937795 //歌词接口
export function songLyric(songId){
	//promise对象
 return	uni.request({
		// 地址拼接
		url:`${baseUrl}/lyric?id=${songId}`,
		method:'GET'
	});
}
// http://localhost:3000/song/url?id=483937795 //音源播放地址
export function songUrl(songId){
	//promise对象
 return	uni.request({
		// 地址拼接
		url:`${baseUrl}/song/url?id=${songId}`,
		method:'GET'
	});
}
// http://localhost:3000/search/hot/detail 热词接口
export function searchHot(){
	//promise对象
 return	uni.request({
		// 地址拼接
		url:`${baseUrl}/search/hot/detail`,
		method:'GET'
	});
}
// http://localhost:3000/search?keyword=哪里都是你 搜索词
export function searchWord(word){
	//promise对象
 return	uni.request({
		// 地址拼接
		url:`${baseUrl}/search?keywords=${word}`,
		method:'GET'
	});
}
//http://localhost:3000/search/suggest?keywords=哪里都是你&type=mobile 下拉提示接口
//搜索建议
export function searchSuggest(word){
	return uni.request({
		url : `${baseUrl}/search/suggest?keywords=${word}&type=mobile`,
		method : 'GET'
	})
}