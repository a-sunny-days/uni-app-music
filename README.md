# uni-app music

#### 介绍
仿网易云小程序 基于Vue.js开发跨平台的前端框架uni-app，封装了后端接口，使用vuex的数据共享进行歌曲切换，调用api进行渲染数据到页面上。包括：首页分类，列表页，详情页，播放器，推荐音乐，评论区，以及搜索。


#### 软件架构
软件架构说明


#### 安装教程

1.  git clone https://gitee.com/a-sunny-days/uni-app-music.git


#### 使用说明

1.  hbuilder 直接运行即可

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
